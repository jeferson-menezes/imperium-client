
import services from '@/http'
import * as types from './mutation_types'

export const ActionDoSaveCategoria = ({ dispatch }, payload) => {
    return services.category.salvar(payload).then(res => {
        dispatch('ActionAddCategoria', res.body)
    })
}

export const ActionDoUpdateCategoria = ({ dispatch }, payload) => {
    return services.category.atualizar({ id: payload.id }, payload.body).then(res => {
        dispatch('ActionFindListCategorias')
    })
}

export const ActionDoDeleteCategoria = ({ dispatch }, payload) => {
    return services.category.deletar({ id: payload}).then(res => {
        dispatch('ActionFindListCategorias')
    })
    
}

export const ActionFindListCategorias = ({ commit }) => {
    services.category.listar().then(res => {
        commit(types.SET_CATEGORIAS, res.body)
    })
}

export const ActionAddCategoria = ({ commit }, payload) => {
    commit(types.ADD_CATEGORIA, payload)
}