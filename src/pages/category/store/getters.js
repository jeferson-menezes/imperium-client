export const getDespesas = ({ categorias }) => {
    return categorias.filter((element) => element.tipo === "DESPESA")
}
export const getReceitas = ({ categorias }) => {
    return categorias.filter((element) => element.tipo === "RECEITA")
}