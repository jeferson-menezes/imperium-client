import * as types from './mutation_types'

export default {
    [types.SET_CATEGORIAS](state, payload) {
        state.categorias = payload
    },
    [types.ADD_CATEGORIA](state, payload) {
        state.categorias.push(payload)
    },
    [types.SET_CATEGORIA](state, payload) {
        state.categorias
    }
}