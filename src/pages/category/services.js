export default {
  salvar: { method: 'post', url: 'categorias' },
  listar: { method: 'get', url: 'categorias' },
  atualizar: { method: 'put', url: 'categorias/{id}' },
  deletar: { method: 'delete', url: 'categorias/{id}' }
}
