export default [
    {
      path: '/categorias',
      name: 'categorias',
      meta: { icon: 'category', title: 'Categorias' },
      component: () => import(/* webpackChunkName: "home" */ './Categorias.vue')
    }
  ]
  