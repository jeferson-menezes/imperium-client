/* CARTOES */
import services from '@/http'
import * as types from './mutation_types'

export const ActionDoListBandeiras = ({ commit }) => {
    services.card.listarBandeitas().then(res => {
        commit(types.SET_BANDEIRAS, res.body)
    })
}

export const ActionDoListAllCartoes = ({ commit }, payload) => {
    services.card.listarAll(payload).then(res => {
        commit(types.SET_CARTOES, res.body)
    })
}
export const ActionDoSaveCartao = ({ commit }, payload) => {
    return services.card.salvar(payload).then(res => {
        commit(types.ADD_CARTAO, res.body)
    })
}

export const ActionDoCompraCartao = ({ commit }, payload) => {
    return services.card.addCompra(payload).then(res => {
        console.log(res)
    })
}