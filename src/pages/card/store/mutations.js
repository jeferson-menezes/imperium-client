import * as types from './mutation_types'

export default {
    [types.SET_BANDEIRAS](state, payload) {
        state.bandeiras = payload
    },
    [types.SET_CARTOES](state, payload) {
        state.cartoes = payload
    },
    [types.ADD_CARTAO](state, payload){
        state.cartoes.push(payload)
    }
}