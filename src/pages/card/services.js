/** CARTOES */
export default {
    listarBandeitas: { method: 'get', url: 'cartoes/bandeiras' },
    salvar: { method: 'post', url:'cartoes'},
    listarAll: { method: 'get', url:'cartoes/all/usuario/{id}'},
    addCompra: { method: 'post', url:'cartoes/compra'}
  }
  