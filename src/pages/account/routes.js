export default [
  {
    path: '/contas',
    name: 'contas',
    meta: { icon: 'account_balance', title: 'Contas' },
    component: () => import(/* webpackChunkName: "contas" */ './Contas')
  }
]
