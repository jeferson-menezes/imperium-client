/* CONTAS */
import services from '@/http'
import * as types from './mutation_types'

export const ActionFindListContas = ({ commit }, payload) => {
    services.account.listar(payload).then(res => {
        commit(types.SET_CONTAS, res.body)
    })
}

export const ActionFindListAllContas = ({ commit }, payload) => {
    services.account.listarAll(payload).then(res => {
        commit(types.ADD_ALLCONTAS, res.body)
    })
}

export const ActionFindListTiposConta = ({ commit }) => {
    services.account.listarTiposConta().then(res => {
        commit(types.SET_TIPO_CONTAS, res.body)
    })
}

export const ActionDoSaveConta = ({ commit }, payload) => {
    return services.account.salvar(payload).then(res => {
        commit(types.ADD_CONTA, res.body)
    })
}

export const ActionTransfers = ({dispatch}, payload) => {
    return services.account.transfere(payload)
}

export const ActionDoUpdateConta= ({dispatch}, payload) =>{
    return services.account.atualizar({id:payload.id},payload.body)
}


export const ActionDoDeleteConta= ({dispatch}, payload) =>{
    return services.account.deletar(payload)
}