import * as types from './mutation_types'

export default {
    [types.SET_CONTAS](state, payload) {
        state.contas = payload
    },
    [types.ADD_CONTA](state, payload) {
        state.contas.content.push(payload)
        state.contas.totalElements++
        state.contas.numberOfElements++
    },
    [types.SET_TIPO_CONTAS](state, payload) {
        state.tiposConta = payload
    },
    [types.ADD_ALLCONTAS](state, payload){
        state.allContas = payload
    }
}