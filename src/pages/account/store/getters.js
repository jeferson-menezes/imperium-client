/* CONTAS */
export const getTotalSaldo = ({ allContas }) => {
    return allContas.reduce((acc, element) => acc + element.saldoInicial, 0.0)
}