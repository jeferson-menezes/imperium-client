/** CONTAS */
export default {
  listar: { method: 'get', url: 'contas/usuario/{id}?pagina={pagina}&qtd={qtd}' },
  listarAll: { method: 'get', url: 'contas/all/usuario/{id}' },
  salvar: { method: 'post', url: 'contas' },
  listarTiposConta: { method: 'get', url: 'tipoContas' },
  transfere: { method: 'put', url: 'contas/transfere' },
  atualizar: {method: 'put', url:'contas/{id}'},
  deletar: {method: 'delete', url:'contas/{id}'}
}
