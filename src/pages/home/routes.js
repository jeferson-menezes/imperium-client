export default [
  {
    path: '/',
    name: 'home',
    meta: { icon: 'dashboard', title: 'Home' },
    component: () => import(/* webpackChunkName: "home" */ './Home')
  }
]
