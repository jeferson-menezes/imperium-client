/* RECEITAS */
import services from '@/http'
import * as types from './mutation_types'

export const ActionDoSaveReceitas = ({ commit }, payload) => {
    return services.recipe.salvar(payload).then(res => {
        console.log("Res receita: ", res);
        // commit(types.ADD_RECEITA, res.body)
    })
}

export const ActionDoFindListReceitas = ({ commit }, payload) => {
    services.recipe.listar(payload).then(res => {
        commit(types.SET_RECEITAS, res.body)
    })
}

export const ActionDoUpdateReceita = ({ commit }, payload) => {
    return services.recipe.atualizar({ id: payload.id }, payload.body).then(res => {
        console.log("Res update: ", res);
    })
}

export const ActionDoConfirmReceive = ({ dispatch }, payload) => {
    return services.recipe.receber({ id: payload }, "").then(res => {
        console.log("Res recebimento: ", res);
    })
}

export const ActionDoUpdateConta = ({ dispatch }, payload) => {
    return services.recipe.alterarConta({ id: payload.id }, payload.contaId).then(res => {
        console.log("Res altera conta", res);
    })
}

export const ActionDoDeleteReceive = ({dispatch}, payload) => {
    return services.recipe.deletar({id: payload}).then(res =>{
        console.log("Res delete", res);
        
    })
}



