import * as types from './mutation_types'

export default {
    [types.SET_RECEITAS](state, payload) {
        state.receitas = payload
    },
    [types.ADD_RECEITA](state, payload) {
        state.receitas.content.push(payload)
        state.receitas.totalElements++
        state.receitas.numberOfElements++
    },
    [types.ADD_ALLRECEITAS](state, payload){
        state.allReceitas = payload
    }
}