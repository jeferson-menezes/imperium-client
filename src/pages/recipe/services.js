export default {
  salvar: { method: 'post', url: 'receitas' },
  listar: { method: 'get', url: 'receitas/usuario/{id}?page={page}&size={size}' },
  atualizar: { method: 'put', url: 'receitas/{id}' },
  receber: { method: 'patch', url: 'receitas/{id}' },
  alterarConta: { method: 'patch', url: 'receitas/{id}/alteraconta' },
  deletar: { method: 'delete', url: 'receitas/{id}' }
}
