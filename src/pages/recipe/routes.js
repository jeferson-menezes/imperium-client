export default [
    {
      path: '/receitas',
      name: 'receitas',
      meta: { icon: 'show_chart', title: 'Receitas' },
      component: () => import(/* webpackChunkName: "receitas" */ './Receitas')
    }
  ]
  