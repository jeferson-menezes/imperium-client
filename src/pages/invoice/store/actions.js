/* FATURAS */
import services from '@/http'
import * as types from './mutation_types'

export const ActionDoListAllFaturas = ({ dispatch }, payload) => {
    services.invoice.listarAll(payload).then(res => {
        dispatch('ActionSortFaturas', res.body)
    })
}
export const ActionSortFaturas = ({ commit }, payload) => {
    payload.sort((a, b) => {
        return a.fechamento < b.fechamento ? -1 : a.fechamento > b.fechamento ? 1 : 0
    })
    payload.reverse()
    commit(types.SET_FATURAS, payload)
}
