import * as types from './mutation_types'

export default {
    [types.SET_FATURAS](state, payload) {
        state.faturas = payload
    }
}