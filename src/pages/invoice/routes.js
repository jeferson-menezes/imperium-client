export default [
    {
      path: '/cartoes',
      name: 'cartoes',
      meta: { icon: 'credit_card', title: 'Cartoes' },
      component: () => import(/* webpackChunkName: "cartoes" */ './Cartoes')
    }
  ]
  