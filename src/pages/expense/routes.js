export default [
  {
    path: '/despesas',
    name: 'despesas',
    meta: { icon: 'shopping_cart', title: 'Despesas' },
    component: () => import(/* webpackChunkName: "despesas" */ './Despesas')
  }
]
