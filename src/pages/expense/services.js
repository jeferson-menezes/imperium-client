export default {
    salvar: { method: 'post', url: 'despesas' },
    listar: { method: 'get', url: 'despesas/usuario/{id}?page={page}&size={size}' },
    pagar: { method: 'patch', url: 'despesas/{id}' },
    alterarConta: { method: 'patch', url: 'despesas/{id}/alteraconta' },
    deletar: { method: 'delete', url: 'despesas/{id}' },
    atualizar: { method: 'put', url: 'despesas/{id}' }
  }
  