/* DESPESAS */
import services from '@/http'
import * as types from './mutation_types'


export const ActionDoSaveDespesa = ({ commit }, payload) => {
    return services.expense.salvar(payload).then(res => {
        console.log("Res add despesa: ", res);
        // commit(types.ADD_RECEITA, res.body)
    })
}

export const ActionDoFindListDespesas = ({ commit }, payload) => {
    services.expense.listar(payload).then(res => {
        commit(types.SET_DESPESAS, res.body)
    })
}

export const ActionDoConfirmPayment = ({ dispatch }, payload) => {
    return services.expense.pagar({ id: payload }, "").then(res => {
        console.log("Res recebimento: ", res);
    })
}

export const ActionDoUpdateConta = ({ dispatch }, payload) => {
    return services.expense.alterarConta({ id: payload.id }, payload.contaId).then(res => {
        console.log("Res altera conta", res);
    })
}

export const ActionDoDeleteDespesa = ({dispatch}, payload) => {
    return services.expense.deletar({id: payload}).then(res =>{
        console.log("Res delete", res);        
    })
}

export const ActionDoUpdateDespesa = ({ commit }, payload) => {
    return services.expense.atualizar({ id: payload.id }, payload.body).then(res => {
        console.log("Res update: ", res);
    })
}