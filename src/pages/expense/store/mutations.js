import * as types from './mutation_types'

export default {
    [types.SET_DESPESAS](state, payload) {
        state.despesas = payload
    },
    [types.ADD_DESPESA](state, payload) {
        state.despesas.content.push(payload)
        state.despesas.totalElements++
        state.despesas.numberOfElements++
    },
    [types.ADD_ALLDESPESAS](state, payload){
        state.allDespesas = payload
    }
}