export const hasToken = ({ token }) => !!token
export const isLogged = ({ logged }) => logged