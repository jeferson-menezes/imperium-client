export default [
  {
    name: 'login',
    path: '/login',
    meta: { icon: 'login', title: 'Login' },
    component: () => import(/* webpackChunkName: "login" */ './pages/Login')
  }
]
