import { routes as home } from '../pages/home'
import { routes as auth } from '../modules/auth'
import { routes as account } from '../pages/account'
import { routes as expense } from '../pages/expense'
import { routes as category } from '../pages/category'
import { routes as recipe } from '../pages/recipe'
import { routes as card } from '../pages/card'


export default [
  ...auth,
  ...home,
  ...account,
  ...expense,
  ...category,
  ...recipe,
  ...card
]
