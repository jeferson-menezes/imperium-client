import { store as auth } from '@/modules/auth'
import { store as category } from '@/pages/category'
import { store as account } from '@/pages/account'
import { store as recipe } from '@/pages/recipe'
import { store as expense } from '@/pages/expense'
import { store as card } from '@/pages/card'
import { store as invoice} from '@/pages/invoice'


export default {
  auth, 
  category,
  account,
  recipe,
  expense,
  card,
  invoice
}
