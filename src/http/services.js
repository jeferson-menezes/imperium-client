import { services as auth } from '@/modules/auth'
import { services as category } from '@/pages/category'
import { services as account } from '@/pages/account'
import { services as recipe } from '@/pages/recipe'
import { services as expense } from '@/pages/expense'
import { services as card } from '@/pages/card'
import { services as invoice } from '@/pages/invoice'


export default {
  auth,
  category,
  account,
  recipe,
  expense,
  card,
  invoice
}
